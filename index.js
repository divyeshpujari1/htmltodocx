'use strict';
const html2docx = require('html-to-docx');
const fs = require('fs');

// HTML string
const htmlString = fs.readFileSync('./template.html', 'utf-8');

// HTML 2 DOC
html2docx(htmlString).then((docContent) => {
  fs.writeFileSync('./demo.docx', docContent);
}).catch((e) => {
  console.error('An error occurred while generating the doc file', e)
});